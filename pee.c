#define _POSIX_C_SOURCE 200809L
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Includes new line character */
#define MAX_LINE_LEN 8192

bool
drop_permisions(void)
{
	gid_t gid;
	uid_t uid;

	/* When invoked with sudo, getuid() and getgid() will return 0 */
#if defined(_GNU_SOURCE)
	const char *sudo_gid = secure_getenv("SUDO_GID");
	const char *sudo_uid = secure_getenv("SUDO_UID");
#else
	const char *sudo_gid = getenv("SUDO_GID");
	const char *sudo_uid = getenv("SUDO_UID");
#endif

	if (sudo_gid && sudo_uid) {
		uid = strtoll(sudo_uid, NULL, 10);
		gid = strtoll(sudo_gid, NULL, 10);
	} else {
		uid = getuid();
		gid = getgid();
	}

	if (uid != geteuid() || gid != getegid()) {
		if (setgid(gid) != 0) {
			fputs("Unable to drop root group, refusing to start\n", stderr);
			return false;
		}

		if (setuid(uid) != 0) {
			fputs("Unable to drop root user, refusing to start\n", stderr);
			return false;
		}
	}

	if (setgid(0) != -1 || setuid(0) != -1) {
		fputs("Unable to drop root, refusing to start\n"
			"(we shouldn't be able to restore it after setuid)\n", stderr);
		return false;
	}

	return true;
}

int
main(int argc, char *argv[]) {
	if (argc < 2) {
		fprintf(stderr,
			"Usage: %s [command]...\n"
			"Read input from stdin and sent it to command(s)\n", argv[0]);
		return EXIT_FAILURE;
	}

	if (!drop_permisions())
		return EXIT_FAILURE;

	int num_cmds = argc - 1;

	struct sigaction sa = {.sa_handler = SIG_IGN};
	sigaction(SIGPIPE, &sa, NULL);

	FILE **files = calloc(num_cmds, sizeof(FILE *));

	for (int i = 0; i < num_cmds; i++) {
		files[i] = popen(argv[i + 1], "w");
	}

	char line[MAX_LINE_LEN];
	while(fgets(line, MAX_LINE_LEN, stdin)) {
		for (int i = 0; i < num_cmds; i++) {
			fprintf(files[i], "%s", line);
			fflush(files[i]);
		}
	}

	for (int i = 0; i < num_cmds; i++)
		pclose(files[i]);

	free(files);
}
